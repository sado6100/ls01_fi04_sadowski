package Wurscht;

import javafx.print.PrintColor;
import javafx.scene.paint.Color;

public class Ausgabeformatierung {
	public static void main(String[] args) {
		
		System.out.print("   **"+ "\n" + "*      *"+"\n"+ "*      *"+"\n"+"   **\n");
		
		
		
		System.out.printf("0! " + "=" + " =" + " 1\n" );
		System.out.printf("1! " + "=" + " 1 =" + " 1\n" );
		System.out.printf("2! " + "=" + " 1 * 2 =" + " 2\n" );
		System.out.printf("3! " + "=" + " 1 * 2 * 3 =" + " 6\n" );
		System.out.printf("4! " + "=" + " 1 * 2 * 3 * 4 =" + " 24\n" );
		System.out.printf("5! " + "=" + " 1 * 2 * 3 * 4 * 5 =" + " 120\n" );
	}

}
