class Mathe {
    public static void main(String[] args) {
        double x1 = 5.0;
        System.out.printf("Das Quadrat von %6.2f ist %6.2f\n", x1, quadrat(x1));
    }

    static double quadrat(double x) {
        return x * x;
    }
}