﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       //double Variablen
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       do {
    	   System.out.println("------------------");
    	   System.out.println("Neue Fahrkarte");
    	   System.out.println("------------------\n");
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
    	   warte(1000);
       
    	   // Geldeinwurf
    	   // -----------
    	   eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
    	   // Fahrscheinausgabe
    	   // -----------------
    	   fahrkartenAusgeben();
       
       

    	   // Rückgeldberechnung und -Ausgabe
    	   // -------------------------------
    	   rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);

    	   System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          	  "vor Fahrtantritt entwerten zu lassen!\n"+
                              "Wir wünschen Ihnen eine gute Fahrt.");
       } while (true);
    }
    public static double fahrkartenbestellungErfassen() {
    	boolean done = true;
    	double preisFuerEinTicket = 0;
    	int lengthA = 10;
    	Scanner tastatur = new Scanner(System.in);
    	String[] name = new String[lengthA];
    	Double[] preis = new Double[lengthA];
    	
    	name[0] = "Einzelfahrschein Berlin AB";
    	name[1] = "Einzelfahrschein Berlin BC";
    	name[2] = "Einzelfahrschein Berlin ABC";
    	name[3] = "Kurzstrecke";
    	name[4] = "Tageskarte Berlin AB";
    	name[5] = "Tageskarte Berlin BC";
    	name[6] = "Tageskarte Berlin ABC";
    	name[7] = "Kleingruppen-Tageskarte Berlin AB";
    	name[8] = "Kleingruppen-Tageskarte Berlin BC";
    	name[9] = "Kleingruppen-Tageskarte Berlin ABC";
    	
    	preis[0] = 2.90;
    	preis[1] = 3.30;
    	preis[2] = 3.60;
    	preis[3] = 1.90;
    	preis[4] = 8.60;
    	preis[5] = 9.00;
    	preis[6] = 9.60;
    	preis[7] = 23.50;
    	preis[8] = 24.30;
    	preis[9] = 24.90;
    	
    	for(int i = 0; i < name.length; i++) {
    		System.out.println(i+1 + "\t" + name[i] + "\t" + preis[i] + "€");
    	}
    	System.out.printf("Geben Sie die Auswahl ein: ");
    	int benutzerwahl = tastatur.nextInt() - 1;
    	boolean eingabe = false;
    	while (!eingabe) {
    		if (benutzerwahl < 0) {
    			System.out.println("Wahl zu niedrig. Bitte geben Sie eine neue Auswahl ein: ");
    			benutzerwahl = tastatur.nextInt() - 1;
    		}
    		else if (benutzerwahl > preis.length - 1) {
    			System.out.println("Wahl zu hoch. Bitte geben Sie eine neue Auswahl ein: ");
    			benutzerwahl = tastatur.nextInt() - 1;
    		}
    		else {
    			eingabe = true;
    			System.out.printf("Auswahl: %s\n", name[benutzerwahl]);
    			preisFuerEinTicket = preis[benutzerwahl];
    		}
    	}
    	
    	
    	int TicketAnzahl;
    	double zuZahlenderBetrag = 0;
        System.out.printf("Anzahl der Tickets: ");
        TicketAnzahl = tastatur.nextInt();
        boolean TicketNotValid = true;
        
        
        while (TicketNotValid == true) {
         if(TicketAnzahl > 10) {
        	TicketNotValid = true;
        	System.out.println("Zu hoher Ticketwert. Bitte eine Ticketanzahl von 1 bis 10 angeben");
        	 try {
           	   Thread.sleep(700);
              } catch (InterruptedException e) {
           	   e.printStackTrace();
    		 }
        	System.out.printf("Anzahl der Tickets: ");
            TicketAnzahl = tastatur.nextInt();
           
         }
         else if(TicketAnzahl < 1) {
        	TicketNotValid = true;
        	System.out.println("Zu niedriger Ticketwert.  Bitte eine Ticketanzahl von 1 bis 10 angeben");
        	 try {
           	   Thread.sleep(700);
              } catch (InterruptedException e) {
           	   e.printStackTrace();
    		  }
        	System.out.printf("Anzahl der Tickets: ");
            TicketAnzahl = tastatur.nextInt();
           
          }
          else {
        	TicketNotValid = false;
        	zuZahlenderBetrag = preisFuerEinTicket * TicketAnzahl;  
          }
         
        }
       return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	Scanner tastatur = new Scanner(System.in);
    	
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	  double Rest = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	  System.out.printf("Noch zu zahlen: " + "%.2f" + " EURO\n", Rest);
     	  System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	  eingeworfeneMünze = tastatur.nextDouble();
          eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
       return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) //int counter
        {
           System.out.print("=");
           try {
        	   Thread.sleep(250);
           } catch (InterruptedException e) {
 		// TODO Auto-generated catch block
        	   e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    	
    }
    
    public static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
    	double rückgabebetrag;
    	
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; //double
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
              muenzenAusgeben(2, "EURO");
 	          rückgabebetrag -= 2.0;
             }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
              muenzenAusgeben(1, "EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
              muenzenAusgeben(50, "CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
              muenzenAusgeben(20, "CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
              muenzenAusgeben(10, "CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag > 0.04)// 5 CENT-Münzen
            {
             muenzenAusgeben(5, "CENT");
             rückgabebetrag -= 0.05;
  	          
            }
        }
    }
   public static void warte(int millisekunden) {
    	
        try {
     	   Thread.sleep(millisekunden);
        } catch (InterruptedException e) {
		// TODO Auto-generated catch block
     	   e.printStackTrace();
        }
   }
        
   public static void muenzenAusgeben(int betrag, String einheit) {
       System.out.printf("%d %s\n", betrag, einheit);
        	
        
    }    
        
}